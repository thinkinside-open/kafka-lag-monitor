# Kafka Lag Monitor

This project is aimed at monitoring the lag of Kafka consumers.

Supports monitoring consumer handled by Kafka itself (i.e. *consumer groups*) as well as consumer using Zookeeper to store committed offset.

Requires Kafka brokers >= 0.10.2.

## Configuration

The application is configured using the HOCON format.

The minimal configuration requires setting the following values in `application.conf`:

```
thinkin.kafka.monitor {
  kafkaServers = "kafka-host1:9092,kafka-host2:9092"
  
  stores {
    consumerGroup = "consumer1,consumer2"
  }
}

```

All configuration values can also be set using environment variables to simplify usage with Docker.

| Setting | Default | Description |
|---------|---------|-------------|
| thinkin.kafka.monitor.interval <br> MONITOR_INTERVAL | 30 seconds | Lag computation interval |
| thinkin.kafka.monitor.kafkaServers <br> KAFKA_SERVERS | null | Kafka server endpoints to use (Required) |
| thinkin.kafka.monitor.zookeeperServers <br> ZOOKEEPER_SERVER | null | Zookeeper servers to use (Required only if Zookeeper based stores are configured) |
| thinkin.kafka.monitor.stores.consumerGroup <br>  STORES_CONSUMER_GROUP | | Comma separated list of consumer groups to monitor |
| thinkin.kafka.monitor.stores.zookeeper <br> STORES_ZOOKEEPER | | Comma separated list of definition with format: `<name>:<path>` | 
| thinkin.kafka.monitor.output.console.enabled <br> MONITOR_OUTPUT_CONSOLE_ENABLED | true | Whether to enable console output |
| thinkin.kafka.monitor.output.elasticsearch.enabled <br> MONITOR_OUTPUT_ELASTICSEARCH_ENABLED | false | Whether to enable Elasticsearch output |
| thinkin.kafka.monitor.output.elasticsearch.servers <br> MONITOR_OUTPUT_ELASTICSEARCH_SERVERS | null | The Elasticsearch server(s) to use (Required only if Elasticsearch output is enabled) |
| thinkin.kafka.monitor.output.elasticsearch.indexPattern <br>  MONITOR_OUTPUT_ELASTICSEARCH_INDEX_PATTERN | kafka-lag-monitor-%Y-%M-%D | The pattern used to identify the Elasticsearc index. Substitutions: `%Y => year`, `%M => month`, `%D => day` |
| thinkin.kafka.monitor.output.elasticsearch.kafka-lag <br> MONITOR_OUTPUT_ELASTICSEARCH_DOC_TYPE | kafka-lag | The Elasticsearch document type to use to store data inside the index |
| thinkin.kafka.monitor.alerts.enabled <br> ALERTS_ENABLED | false | Whether to enable alerts for consumer in state *stuck* (offset not increasing) or *lag-increasing* (offset increasing slower than new messages are coming in)
| thinkin.kafka.monitor.alerts.repeatDuration <br> ALERTS_REPEAT_DURATION | 1 day | How often re-emit notifications |
| thinkin.kafka.monitor.alerts.stuckMinDuration <br> ALERTS_STUCK_MIN_DURATION | 5 min | Min time after which considering a consumer *stuck* if at least one partition's offset are not increasing |
| thinkin.kafka.monitor.alerts.lagIncreasingDuration <br> ALERTS_LAG_INCREASING_DURATION | 1 hour | Min time after which considering a consumer in *lag-increasing* state |
| thinkin.kafka.monitor.alerts.emitters.console.enabled <br> ALERTS_EMITTERS_CONSOLE_ENABLED | true | Whether to enable the console alerts emitter |
| thinkin.kafka.monitor.alerts.emitters.slack.enabled <br> ALERTS_EMITTERS_SLACK_ENABLED | false | Whether to enable the slack based alerts emitter |
| thinkin.kafka.monitor.alerts.emitters.slack.webhookUri <br> ALERTS_EMITTERS_SLACK_WEBHOOK_URI | | **REQUIRED** The Slack's webhooks endpoint to use |
| thinkin.kafka.monitor.alerts.emitters.slack.sourceId <br> ALERTS_EMITTERS_SLACK_SOURCE_ID | default | The source label used in the Slack message (useful to differentiate between multiple installation) |
| thinkin.kafka.monitor.alerts.emitters.slack.timeout <br> ALETRS_EMITTERS_SLACK_TIMEOUT | 10 seconds | Slack's webhook request timeout |

 
 
 ## Zookeeper offset monitoring
 
 When the setting `thinkin.kafka.monitor.stores.zookeeper` is set to a value like `zconsumer1:/app/kafka/offset`, 
 the path `/app/kafka/offset` will be fetched at every interval and parsed to retrieve the consumer's committed offset map.
 
 Currently the only format supported for storing the offset map is a comma separated of: `<topic>:<partition>=<offset>`. 
 
 ## Elasticsearch output
 
 The collected lag can be stored into Elasticsearch to easily visualize them using something like Kibana.
 
 If you want to configure the index template you can use the following definition (compatible with Elasticsearch >= 5.6):
 
 ```
 {
   "template": "kafka-lag-monitor-*",
   "settings": {
     "number_of_shards": 1
   },
   "mappings": {
     "kafka-lag": {
       "properties": {
         "@timestamp": { "type": "date" },
         "type": { "type":  "keyword" },
 
         "consumer": {
           "type": "string",
           "fields": {
             "keyword": { "type": "keyword" }
           }
         },
 
         "topic": {
           "type": "string",
           "fields": {
             "keyword": { "type": "keyword" }
           }
         },
         "partition": { "type":  "integer" },
         "lag": { "type": "long" },
         "offset": { "type": "long" }
       }
     }
   }
 }
```
 
The index will be populated with 2 kind of document:
 
1. Total lag entry:

Specifies the total lag of the consumer, computed as the sum of all the lag for specific topic-partition tuples the consumer is subscribed to.
```
{
    "@timestamp": 1564739402169,
    "type": "totalLag",
    "consumer": "consumerGroup1",
    "lag": 100
  }
```
 
2. Topic partition lag entry:

Specifies the lag of the consumer for a specific topic-partition tuple
 ```
 {
     "@timestamp": 1564739402256,
     "type": "partitionLag",
     "consumer": "comsumerGroup1",
     "topic": "topicName",
     "partition": 0,
     "lag": 50,
     "offset": 100
   }
 ```