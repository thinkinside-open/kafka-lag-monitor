name := "kafka-lag-monitor"
organization := "thinkin.io"
scalaVersion := "2.11.12"

val gitVersion: String = {
  import scala.sys.process._
  ("git describe --tags --always --dirty" !!).trim
}

version := gitVersion

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xlint",
  "-Xlint:missing-interpolator",
  "-Xlint:private-shadow",
  "-Xlint:type-parameter-shadow",
  "-Xlint:unsound-match",
  "-Xlint:infer-any",
  "-Xlint:nullary-override",
  "-Ywarn-unused",
  "-Ywarn-adapted-args",
  "-Ywarn-inaccessible",
  "-Yopt-warnings:_",
  "-Ywarn-dead-code",
  "-Xmax-classfile-name","100" //Fixes "Filename too long" when building inside Docker containers
)


libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % "0.11.1",
  "com.typesafe" % "config" % "1.3.4",
  "com.typesafe.akka" %% "akka-stream" % "2.5.23",
  "com.typesafe.akka" %% "akka-http"   % "10.1.9",
  "org.apache.kafka" % "kafka-clients" % "2.3.0",
  "com.101tec" % "zkclient" % "0.11",
  "com.sksamuel.elastic4s" %% "elastic4s-http" % "6.5.2",
  "org.slf4j" % "slf4j-api" % "1.7.26",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.slf4j" % "jcl-over-slf4j" % "1.7.26"
)

excludeDependencies ++= Seq(
  "log4j" % "log4j",
  "org.slf4j" % "slf4j-log4j12",
  "commons-logging" % "commons-logging"
)

enablePlugins(PackPlugin, DockerPlugin)

packMain := Map("kafka-lag-monitor" -> "io.thinkin.kafka.KafkaLagMonitorRunner")
packResourceDir += ((sourceDirectory in Compile).value / "resources-pack" -> "")
packExtraClasspath := Map("kafka-lag-monitor" -> Seq("$CLASSPATH"))

dockerfile in docker := {
  val tarFile: File = packArchiveTbz.value
  val basename = tarFile.name.replaceAll("""(?i)(.*)\.tar\.bz2""", "$1")
  val dest = s"/app"

  val aliasDir = s"${dest}/${name.value}"
  new Dockerfile {
    from("openjdk:8-slim")

    runRaw(s"mkdir -p ${dest}")
    add(tarFile, dest)
    runRaw(s"ln -s /${dest}/${basename} ${aliasDir}")

    workDir(aliasDir)
    cmd(s"${aliasDir}/bin/kafka-lag-monitor")
  }
}

imageNames in docker := Seq(
  ImageName(s"thinkin/${name.value}:${version.value}"),
  ImageName(s"thinkin/${name.value}:latest")
)

bintrayOrganization := Some("thinkinside")
bintrayRepository := "opensource"
licenses += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0"))
