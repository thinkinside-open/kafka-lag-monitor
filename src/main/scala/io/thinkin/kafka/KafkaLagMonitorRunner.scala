/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka

import java.util.Properties

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.{Config, ConfigFactory}
import io.thinkin.kafka.notifications.{ConsoleNotificationEmitter, NotificationConfiguration, NotificationEmitter, NotificationManager, SlackNotificationEmitter}
import io.thinkin.kafka.output.{ConsoleLagOutput, ElasticsearchLagOutput, LagOutput}
import io.thinkin.kafka.store.{CsvOffsetDeserializer, KafkaConsumerOffsetStore, ZookeeperOffsetStore}
import org.I0Itec.zkclient.ZkClient
import org.I0Itec.zkclient.serialize.ZkSerializer
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.ByteArrayDeserializer
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}
import scala.collection.immutable

object KafkaLagMonitorRunner {
  val log = LoggerFactory.getLogger(this.getClass)

  val zookeeperSerializer = new ZkSerializer() {
    override def serialize(data: scala.Any): Array[Byte] = data match {
      case x: String => x.getBytes("UTF-8")
      case _ => throw new IllegalArgumentException("Unexpected data")
    }

    override def deserialize(bytes: Array[Byte]): AnyRef = new String(bytes, "UTF-8")
  }

  def initKafka(config: Config): (AdminClient, KafkaConsumer[String, String]) = {
    if (!config.hasPath("thinkin.kafka.monitor.kafkaServers")) {
      log.error("No Kafka server specified!")
      System.exit(2)
    }

    val props = new Properties()
    props.setProperty("bootstrap.servers", config.getString("thinkin.kafka.monitor.kafkaServers"))
    props.setProperty("key.deserializer", classOf[ByteArrayDeserializer].getName)
    props.setProperty("value.deserializer", classOf[ByteArrayDeserializer].getName)

    val adminClient = AdminClient.create(props)
    val consumer = new KafkaConsumer[String, String](props)

    adminClient -> consumer
  }

  def initKafkaConsumerGroupStores(config: Config, adminClient: AdminClient): Iterable[KafkaConsumerOffsetStore] = {
    val csStoresStr = config.getString("thinkin.kafka.monitor.stores.consumerGroup")

    csStoresStr.split(",").toList.filter(_.nonEmpty).map { csGroupName =>
      log.info(s"Configured Kafka consumer group [${csGroupName}]")
      new KafkaConsumerOffsetStore(csGroupName)(adminClient)
    }
  }

  def initZookeeper(config: Config): ZkClient = {
    if (!config.hasPath("thinkin.kafka.monitor.zookeeperServers")) {
      log.error("No Zookeeper server specified!")
      System.exit(2)
    }

    val client = new ZkClient(config.getString("thinkin.kafka.monitor.zookeeperServers"))
    client.setZkSerializer(zookeeperSerializer)
    client
  }

  def initZookeeperStores(config: Config): Iterable[ZookeeperOffsetStore] = {
    lazy val zkClient = initZookeeper(config)

    val zkStoreRE = """(.+):(.+)""".r
    val zkStoresStr = config.getString("thinkin.kafka.monitor.stores.zookeeper")
    zkStoresStr.split(",").toList.filter(_.nonEmpty).flatMap { s =>
      s match {
        case zkStoreRE(name, path) =>
          log.info(s"Configured Zookeeper consumer [${name}] with path [${path}]")
          Some(new ZookeeperOffsetStore(name, path)(zkClient, CsvOffsetDeserializer))

        case _ =>
          log.error(s"Invalid Zookeeper store configuration [${s}]. Ignoring...")
          None
      }
    }
  }

  def initOutputs(config: Config)(implicit ec: ExecutionContext): Iterable[LagOutput] = {
    val console = if (config.getBoolean("thinkin.kafka.monitor.output.console.enabled")) {
      log.info("Enabled console output")
      Some(ConsoleLagOutput)
    } else None

    val elasticsearch = if (config.getBoolean("thinkin.kafka.monitor.output.elasticsearch.enabled")) {
      if (!config.hasPath("thinkin.kafka.monitor.output.elasticsearch.servers")) {
        log.error("No Elasticsearch server specified!")
        System.exit(2)
      }

      val servers = config.getString("thinkin.kafka.monitor.output.elasticsearch.servers")
      val indexPattern = config.getString("thinkin.kafka.monitor.output.elasticsearch.indexPattern")
      val docType = config.getString("thinkin.kafka.monitor.output.elasticsearch.docType")
      log.info(s"Enabled Elasticsearch output servers=[${servers}] indexPattern=[${indexPattern}] docType=[$docType]")
      Some(new ElasticsearchLagOutput(servers, indexPattern, docType))
    } else None

    List(console, elasticsearch).flatten
  }

  def initAlarmNotifications(config: Config)(implicit system: ActorSystem, mat: Materializer, ec: ExecutionContext): Flow[Iterable[ConsumerLag], Future[Unit], NotUsed] = {
    if (config.getBoolean("thinkin.kafka.monitor.alerts.enabled")) {

      val notificationConf = NotificationConfiguration(
        config.getDuration("thinkin.kafka.monitor.alerts.repeatDuration").toMillis.millis,
        config.getDuration("thinkin.kafka.monitor.alerts.stuckMinDuration").toMillis.millis,
        config.getDuration("thinkin.kafka.monitor.alerts.lagIncreasingDuration").toMillis.millis
      )

      val consoleEmitter = if (config.getBoolean("thinkin.kafka.monitor.alerts.emitters.console.enabled")) {
        Some(ConsoleNotificationEmitter)
      } else None

      val slackEmitter = if (config.getBoolean("thinkin.kafka.monitor.alerts.emitters.slack.enabled")) {
        val webhookUri: Uri = if (config.hasPath("thinkin.kafka.monitor.alerts.emitters.slack.webhookUri")) {
          val uriString = config.getString("thinkin.kafka.monitor.alerts.emitters.slack.webhookUri")
          Try(Uri(uriString)).getOrElse(sys.error(s"Invalid slack notification webhook uri: ${uriString}"))
        } else sys.error("Slack notification emitter requires the definition of the webhook uri")

        val sourceId = config.getString("thinkin.kafka.monitor.alerts.emitters.slack.sourceId")
        val timeout = config.getDuration("thinkin.kafka.monitor.alerts.emitters.slack.timeout").toMillis.millis
        Some(new SlackNotificationEmitter(webhookUri, sourceId, timeout))
      } else None

      val emitters: List[NotificationEmitter] = List(consoleEmitter, slackEmitter).flatten

      require(emitters.nonEmpty, "Alert notification enabled but no emitter configured!")
      log.info(s"Enabling alerts notification with repeatDuration=[${notificationConf.repeatDuration}] stuckMinDuration=[${notificationConf.stuckMinDuration}] lagIncreasingDuration=[${notificationConf.lagIncreasingDuration}] with emitters: ${emitters.map(_.id).mkString(", ")}")

      Flow[Iterable[ConsumerLag]].statefulMapConcat( () => {
        val notificationManager = new NotificationManager(notificationConf, emitters)

        lags: Iterable[ConsumerLag] =>
          immutable.Iterable(notificationManager.process(lags))
      })
    } else {
      Flow[Iterable[ConsumerLag]].map(_ => Future.successful( () ))
    }
  }


  def main(args: Array[String]): Unit = {
    try {
      import scala.concurrent.ExecutionContext.Implicits.global
      val config = ConfigFactory.load()

      val (kafkaAdmin, kafkaConsumer) = initKafka(config)

      val consumerGroupStores: Iterable[KafkaConsumerOffsetStore] = initKafkaConsumerGroupStores(config, kafkaAdmin)
      val zookeeperStores: Iterable[ZookeeperOffsetStore] = initZookeeperStores(config)

      val stores = zookeeperStores ++ consumerGroupStores
      if (stores.isEmpty) {
        log.error("No consumer offset store configured! Quitting!")
        System.exit(2)
      }

      val lagMonitor = new KafkaLagMonitor(kafkaAdmin, kafkaConsumer, stores)

      val logOutput: Iterable[LagOutput] = initOutputs(config)

      implicit val system: ActorSystem = ActorSystem("KafkaMonitorRunner")
      implicit val mat: Materializer = ActorMaterializer()

      val interval = config.getDuration("thinkin.kafka.monitor.interval").toMillis.millis

      val notificationFlow: Flow[Iterable[ConsumerLag], Future[Unit], NotUsed] = initAlarmNotifications(config)


      val done = Source.tick(0.millis, interval, ()).mapAsync(1) { _ =>
        lagMonitor.computeLag()
      }.mapAsync(1) { lags =>
        for {
          _ <- Future.sequence(logOutput.map(_.outputLag(lags))).map(_ => ())
        } yield lags
      }.via(notificationFlow)
        .runWith(Sink.ignore)

      done.andThen {
        case Success(res) =>
          log.error("Kafka monitor pipeline terminated successfully! This should not happen")
          System.exit(1)
        case Failure(ex) =>
          log.error("Kafka monitor pipeline terminated with exception", ex)
          System.exit(1)
      }

      log.info(s"Kafka log monitor started with interval [${interval}]")
    } catch {
      case err: Exception =>
        log.error("Error starting Kafka log monitor", err)
        System.exit(1)
    }
  }
}
