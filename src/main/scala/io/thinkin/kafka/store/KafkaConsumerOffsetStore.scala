/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.store

import org.apache.kafka.clients.admin.AdminClient
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._

class KafkaConsumerOffsetStore(val consumerGroup: String)(adminClient: AdminClient) extends OffsetStore {
  val log = LoggerFactory.getLogger(this.getClass)

  override def getCommittedPartitionOffset(): CommittedPartitionOffsets = {
    val res = adminClient.listConsumerGroupOffsets(consumerGroup)

    val offsetMap = res.partitionsToOffsetAndMetadata()
      .get().entrySet().iterator().asScala.map { e =>
      e.getKey -> e.getValue.offset()
    }.toMap

    CommittedPartitionOffsets(System.currentTimeMillis(), consumerGroup, offsetMap)
  }
}
