/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.store

import org.apache.kafka.common.TopicPartition
import org.slf4j.LoggerFactory

trait OffsetDeserializer {
  def deserialize(data: String): Map[TopicPartition, Long]
}

object CsvOffsetDeserializer extends OffsetDeserializer {
  val log = LoggerFactory.getLogger(this.getClass)

  val topicPartitionOffsetRE = """(.+):(\d+)=(\d+)""".r

  override def deserialize(data: String): Map[TopicPartition, Long] = {
    data.split(",").iterator.flatMap { topicPartitionOffsetStr =>
      topicPartitionOffsetStr match {
        case topicPartitionOffsetRE(topic, partition, offset) =>
          Some(new TopicPartition(topic, partition.toInt) -> offset.toLong)

        case _ =>
          log.warn(s"Invalid zookeeper offset map: '${topicPartitionOffsetStr}'")
          None
      }
    }.toMap
  }
}