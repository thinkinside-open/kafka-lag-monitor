/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.notifications

sealed trait NotificationState {
  def update(consumerAlarmState: ConsumerAlarmState, conf: NotificationConfiguration): Option[NotificationState]
}

sealed trait NotificationLevel
object NotificationLevel {
  case object INFO extends NotificationLevel
  case object WARNING extends NotificationLevel
  case object ERROR extends NotificationLevel
}

sealed trait EmittableNotificationState extends NotificationState {
  val id: String
  val timestamp: Long
  val level: NotificationLevel
}

object NotificationState {
  case class CandidateStuckNotificationState(initialStuckTimestamp: Long) extends NotificationState {
    override def update(consumerAlarmState: ConsumerAlarmState, conf: NotificationConfiguration): Option[NotificationState] = {
      lazy val now = System.currentTimeMillis()

      if (consumerAlarmState.stuckAlert) {
        if (now - initialStuckTimestamp > conf.stuckMinDuration.toMillis) Some(StuckNotificationState(initialStuckTimestamp))
        else None
      } else if (consumerAlarmState.lagIncreasingAlert) {
        Some(CandidateLagIncreasingNotificationState(now))
      } else Some(OkNotificationState(now))
    }
  }

  case class StuckNotificationState(initialStuckTimestamp: Long) extends EmittableNotificationState {
    override val id: String = "stuck"
    override val timestamp: Long = initialStuckTimestamp
    override val level: NotificationLevel = NotificationLevel.ERROR

    override def update(consumerAlarmState: ConsumerAlarmState, conf: NotificationConfiguration): Option[NotificationState] = {
      lazy val now = System.currentTimeMillis()

      if (consumerAlarmState.stuckAlert) None
      else if (consumerAlarmState.lagIncreasingAlert) Some(CandidateLagIncreasingNotificationState(initialStuckTimestamp))
      else Some(OkNotificationState(now))
    }
  }

  case class CandidateLagIncreasingNotificationState(initialLagIncreasingTimestamp: Long) extends NotificationState {
    override def update(consumerAlarmState: ConsumerAlarmState, conf: NotificationConfiguration): Option[NotificationState] = {
      lazy val now = System.currentTimeMillis()

      if (consumerAlarmState.stuckAlert) Some(CandidateStuckNotificationState(now))
      else if (consumerAlarmState.lagIncreasingAlert) {
        if (now - initialLagIncreasingTimestamp > conf.lagIncreasingDuration.toMillis) Some(LagIncreasingNotificationState(initialLagIncreasingTimestamp))
        else None
      } else Some(OkNotificationState(now))
    }
  }

  case class LagIncreasingNotificationState(initialLagIncreasingTimestamp: Long) extends EmittableNotificationState {
    override val id: String = "lag-increasing"
    override val timestamp: Long = initialLagIncreasingTimestamp
    override val level: NotificationLevel = NotificationLevel.WARNING

    override def update(consumerAlarmState: ConsumerAlarmState, conf: NotificationConfiguration): Option[NotificationState] = {
      lazy val now = System.currentTimeMillis()

      if (consumerAlarmState.stuckAlert) Some(CandidateStuckNotificationState(now))
      else if (consumerAlarmState.lagIncreasingAlert) None
      else Some(OkNotificationState(now))
    }
  }

  case class OkNotificationState(initialTimestamp: Long) extends EmittableNotificationState {
    override val id: String = "ok"
    override val timestamp: Long = initialTimestamp
    override val level: NotificationLevel = NotificationLevel.INFO

    override def update(consumerAlarmState: ConsumerAlarmState, conf: NotificationConfiguration): Option[NotificationState] = {
      lazy val now = System.currentTimeMillis()

      if (consumerAlarmState.stuckAlert) Some(CandidateStuckNotificationState(now))
      else if (consumerAlarmState.lagIncreasingAlert) Some(CandidateLagIncreasingNotificationState(now))
      else None
    }
  }
}