/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.notifications

import io.thinkin.kafka.ConsumerLag
import io.thinkin.kafka.notifications.NotificationState.OkNotificationState
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

case class EmittedNotificationState(state: EmittableNotificationState, timestamp: Long) {
  def update(newState: EmittableNotificationState, conf: NotificationConfiguration): Option[EmittedNotificationState] = {
    lazy val now = System.currentTimeMillis()
    if (newState.id == state.id) {
      state match {
        case _: OkNotificationState => None
        case _ if now - timestamp > conf.repeatDuration.toMillis => Some(EmittedNotificationState(state, now))
        case _ => None
      }
    } else Some(EmittedNotificationState(newState, now))
  }
}

object EmittedNotificationState {
  def apply(state: EmittableNotificationState): EmittedNotificationState = {
    EmittedNotificationState(state, System.currentTimeMillis())
  }
}



class NotificationManager(conf: NotificationConfiguration, emitters: Seq[NotificationEmitter])(implicit ec: ExecutionContext) {
  val log = LoggerFactory.getLogger(this.getClass)

  val consumerStates = mutable.Map[String, ConsumerAlarmState]()
  val notificationStates = mutable.Map[String, NotificationState]()
  val emittedNotificationStates = mutable.Map[String, EmittedNotificationState]()

  def emitNotification(group: String, notificationState: EmittedNotificationState, maybePrevNotificationState: Option[EmittedNotificationState])(implicit ec: ExecutionContext): Future[Unit] = {
    log.debug(s"Emitting notification for group [${group}]: ${notificationState}")

    val fs = emitters.map { e =>
      e.emitNotification(group, notificationState, maybePrevNotificationState).recoverWith {
        case e: Exception =>
          log.error(s"Error emitting notification via Emitter [${e}]", e)
          Future.successful( () )
      }
    }

    Future.sequence(fs).map(_ => Unit)
  }

  def process(lags: Iterable[ConsumerLag]): Future[Unit] = {
    lags.foreach { lag =>
      val newState = consumerStates.get(lag.name) match {
        case Some(state) => state.update(lag)
        case None => ConsumerAlarmState(lag)
      }

      consumerStates += lag.name -> newState
      log.debug(s"Group [${lag.name}] lag-increasing state=[${newState.lagIncreasingAlert}], alert state=[${newState.stuckAlert}], stuck partitions=[${newState.stuckPartitions}]")
    }

    val notificationF = consumerStates.iterator.flatMap { case (group, state) =>
      val maybePrevNotificationState = notificationStates.get(group)

      val maybeNewNotificationState = maybePrevNotificationState match {
        case Some(currentNotificationState) =>
          currentNotificationState.update(state, conf)

        case None =>
          Some( NotificationState.OkNotificationState(System.currentTimeMillis()) )
      }

      maybeNewNotificationState.foreach { n =>
        log.debug(s"Group [${group}] transited from state=[${maybePrevNotificationState}] to state=[${n}]")
        notificationStates += group -> n
      }

      val maybeCurrentState = maybeNewNotificationState.orElse(maybePrevNotificationState)

      maybeCurrentState.flatMap {
        case currentState: EmittableNotificationState =>
          val maybePrevEmittedState = emittedNotificationStates.get(group)

          val maybeNewEmittedNotificationState = maybePrevEmittedState match {
            case Some(currentEmittedNotificationState) =>
              currentEmittedNotificationState.update(currentState, conf)

            case None =>
              Some( EmittedNotificationState(currentState) )
          }

          maybeNewEmittedNotificationState.map { newEmittedState =>
            emittedNotificationStates += group -> newEmittedState
            emitNotification(group, newEmittedState, maybePrevEmittedState)
          }
        case _ =>
          None
      }
    }.toList

    Future.sequence(notificationF).map(_ => Unit)
  }
}
