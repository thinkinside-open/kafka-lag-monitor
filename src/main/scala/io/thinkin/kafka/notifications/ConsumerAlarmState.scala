/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.notifications

import io.thinkin.kafka.ConsumerLag
import org.apache.kafka.common.TopicPartition

case class ConsumerAlarmState(lag: ConsumerLag, stuckAlert: Boolean, stuckPartitions: Set[TopicPartition], lagIncreasingAlert: Boolean) {
  def update(newLag: ConsumerLag): ConsumerAlarmState = {
    if (newLag.totalLag == 0) {
      ConsumerAlarmState(newLag, false, Set.empty, false)
    } else {
      // At least one partition is stuck
      val newStuckPartitions = newLag.lagByTopicPartition.filter { case (_, partitionLag) => partitionLag > 0 }.keys.filter { partition =>
        newLag.offsetByTopicPartition.get(partition) == lag.offsetByTopicPartition.get(partition)
      }.toSet

      val newLagIncreasingAlert = newLag.totalLag > lag.totalLag

      val newStuckPartitionIntersection = if (stuckPartitions.nonEmpty) newStuckPartitions intersect stuckPartitions else newStuckPartitions
      val newStuckAlert = newStuckPartitionIntersection.nonEmpty

      ConsumerAlarmState(newLag, newStuckAlert, newStuckPartitionIntersection, newLagIncreasingAlert)
    }
  }
}

object ConsumerAlarmState {
  def apply(lag: ConsumerLag): ConsumerAlarmState = new ConsumerAlarmState(lag, false, Set.empty,false)
}
