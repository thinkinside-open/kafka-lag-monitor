/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.notifications

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneOffset}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, StatusCodes, Uri}
import akka.stream.Materializer
import akka.util.ByteString
import io.circe.Json
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

trait NotificationEmitter {
  val id: String

  def durationToPrettyString(duration: FiniteDuration): String = {
    if (duration.toDays > 0) s"${duration.toDays} days and ${duration.toHours % 24}"
    else if (duration.toHours > 0) s"${duration.toHours} hours and ${duration.toMinutes % 60} minutes"
    else if (duration.toMinutes > 0) s"${duration.toMinutes} minutes and ${duration.toSeconds % 60} seconds"
    else s"${duration.toSeconds} seconds"
  }

  def formattedMessage(group: String, notificationState: EmittedNotificationState, maybePrevNotificationState: Option[EmittedNotificationState]): String = {
    val formattedTime = Instant.ofEpochMilli(notificationState.timestamp).atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME)

    maybePrevNotificationState match {
      case Some(prevNotificationState) =>
        if (prevNotificationState.state.id != notificationState.state.id) {
          val duration = (notificationState.state.timestamp - prevNotificationState.state.timestamp).millis
          s"Group [${group}] transitioned from state [${prevNotificationState.state.id}] to [${notificationState.state.id}] at time ${formattedTime} after ${durationToPrettyString(duration)}"
        } else {
          val duration = (System.currentTimeMillis() - prevNotificationState.state.timestamp).millis
          s"Group [${group}] still in state [${prevNotificationState.state.id}] at time ${formattedTime} for ${durationToPrettyString(duration)}"
        }

      case None =>
        s"Added group [${group}] in state [${notificationState.state.id}] at time ${formattedTime}"
    }
  }

  def emitNotification(group: String, notificationState: EmittedNotificationState, maybePrevNotificationState: Option[EmittedNotificationState]): Future[Unit]
}

object ConsoleNotificationEmitter extends NotificationEmitter {
  override val id: String = "console"

  def emitNotification(group: String, notificationState: EmittedNotificationState, maybePrevNotificationState: Option[EmittedNotificationState]): Future[Unit] = {
    val message = formattedMessage(group, notificationState, maybePrevNotificationState)
    Future.successful {
      notificationState.state.level match {
        case NotificationLevel.INFO => println(s"ALERT [INFO] ${message}")
        case NotificationLevel.WARNING => println(s"ALERT [WARN] ${message}")
        case NotificationLevel.ERROR => println(s"ALERT [DANGER] ${message}")
      }
    }
  }
}

class SlackNotificationEmitter(webhookEndpoint: Uri, sourceId: String, timeout: FiniteDuration)(implicit system: ActorSystem, mat: Materializer, ec: ExecutionContext) extends NotificationEmitter {
  val log = LoggerFactory.getLogger(this.getClass)

  override val id: String = "slack"

  override def emitNotification(group: String, notificationState: EmittedNotificationState, maybePrevNotificationState: Option[EmittedNotificationState]): Future[Unit] = {
    import io.circe.syntax._

    val title = s"Kafka Lag Monitor: [${sourceId} - ${group}] ${notificationState.state.id}"
    val message = formattedMessage(group, notificationState, maybePrevNotificationState)
    val color = notificationState.state.level match {
      case NotificationLevel.INFO => "good"
      case NotificationLevel.WARNING => "warning"
      case NotificationLevel.ERROR => "danger"
    }

    val body: Json = Json.obj(
      "attachments" := Json.arr(
        Json.obj(
          "title" := title,
          "text" := message,
          "color" := color,
          "fields" := Json.arr(
            Json.obj(
              "title" := "Source",
              "value" := sourceId,
              "short" := true
            ),
            Json.obj (
              "title" := "Consumer Group",
              "value" := group,
              "short" := true
            ),
            Json.obj (
              "title" := "State",
              "value" := notificationState.state.id,
              "short" := true
            ),
            Json.obj (
              "title" := "Time",
              "value" := Instant.ofEpochMilli(notificationState.timestamp).atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME),
              "short" := true
            )
          )
        )
      )
    )

    val respF = Http().singleRequest(HttpRequest(method = HttpMethods.POST, uri = webhookEndpoint, entity = HttpEntity(ContentTypes.`application/json`, body.noSpaces))).flatMap {
      case resp if resp.status != StatusCodes.OK =>
        resp.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map { err =>
          log.error(s"Slack webhook responded with error [${resp.status}]: ${err}")
        }

      case resp =>
        resp.discardEntityBytes().future().map( _ => () )
    }.recoverWith {
      case err: Exception =>
        log.error(s"Error while contacting Slack webhook", err)
        Future.successful( () )
    }


    val timeoutF = akka.pattern.after(timeout, system.scheduler)(Future.successful( () => log.error(s"Timeout while sending slack notification for message ${message}") ))
    val wrappedRespF = respF.map( _ => () => ())

    Future.firstCompletedOf( Seq(timeoutF, wrappedRespF) ).map( _.apply() )
  }
}