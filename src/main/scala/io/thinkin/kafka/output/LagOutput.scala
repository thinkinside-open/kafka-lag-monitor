/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.output

import io.thinkin.kafka.ConsumerLag

import scala.concurrent.Future

trait LagOutput {
  def outputLag(lags: Iterable[ConsumerLag]): Future[Unit]
}


object ConsoleLagOutput extends LagOutput {
  override def outputLag(lags: Iterable[ConsumerLag]): Future[Unit] = {
    Future.successful {
      lags.foreach { l =>
        println(s"TotalLag consumer=[${l.name}] value=[${l.totalLag}]")

        l.offsetByTopicPartition.keys.foreach { tp =>
          println(s"TopicPartitionLag consumer=[${l.name}] topic=[${tp.topic()}] partition=[${tp.partition()}] offset=[${l.offsetByTopicPartition.getOrElse(tp, -1l)}] lag=[${l.lagByTopicPartition.getOrElse(tp, -1)}]")
        }
      }
    }
  }
}