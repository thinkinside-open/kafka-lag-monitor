/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka.output

import java.time.{Instant, ZoneOffset, ZonedDateTime}

import com.sksamuel.elastic4s.Indexable
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties}
import io.thinkin.kafka.ConsumerLag
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}

sealed trait ElasticsearchKafkaLagMonitorDoc {
  val timestamp: Long
}

object ElasticsearchKafkaLagMonitorDoc {
  import io.circe._
  import io.circe.syntax._

  case class TotalLag(timestamp: Long, consumer: String, totalLag: Long) extends ElasticsearchKafkaLagMonitorDoc
  case class PartitionLag(timestamp: Long, consumer: String, topic: String, partition: Int, committedOffset: Long, lag: Long) extends ElasticsearchKafkaLagMonitorDoc

  implicit val elasticsearchKafkaLagMonitorDoc: Encoder[ElasticsearchKafkaLagMonitorDoc] = Encoder.instance {
    case tl: TotalLag =>
      Json.obj(
        "@timestamp" := tl.timestamp,
        "type" := "totalLag",
        "consumer" := tl.consumer,
        "lag" := tl.totalLag
      )

    case pl: PartitionLag =>
      Json.obj(
        "@timestamp" := pl.timestamp,
        "type" := "partitionLag",
        "consumer" := pl.consumer,
        "topic" := pl.topic,
        "partition" := pl.partition,
        "lag" := pl.lag,
        "offset" := pl.committedOffset
    )
  }

  implicit def indexableEncoder[T](implicit encoder: Encoder[T]): Indexable[T] = new Indexable[T] {
    override def json(t: T): String = encoder.apply(t).noSpaces
  }
}

class ElasticsearchLagOutput(esHosts: String, indexPattern: String, docType: String)(implicit ec: ExecutionContext) extends LagOutput {
  val log = LoggerFactory.getLogger(this.getClass)

  import com.sksamuel.elastic4s.http.ElasticDsl._
  val client = ElasticClient(ElasticProperties(esHosts))

  private def expandIndexPattern(timestamp: Long): String = {
    val dateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneOffset.UTC)

    indexPattern
      .replaceAll("%Y", dateTime.getYear.toString)
      .replaceAll("%M", dateTime.getMonthValue.toString)
      .replaceAll("%D", dateTime.getDayOfMonth.toString)
  }

  override def outputLag(lags: Iterable[ConsumerLag]): Future[Unit] = {
    import ElasticsearchKafkaLagMonitorDoc._

    val docs: Iterable[ElasticsearchKafkaLagMonitorDoc] = lags.flatMap { l =>
      val totalLag = TotalLag(l.timestamp, l.name, l.totalLag)

      val partitionLags = l.offsetByTopicPartition.keysIterator.map { tp =>
        PartitionLag(l.timestamp, l.name, tp.topic(), tp.partition(), l.offsetByTopicPartition.getOrElse(tp, -1), l.lagByTopicPartition.getOrElse(tp, -1))
      }.toList

      totalLag :: partitionLags
    }

    val executionResp = client.execute(
      bulk(
        docs.map { l =>
          indexInto(expandIndexPattern(l.timestamp), docType).doc(l)
        }
      )
    )

    executionResp.map {
      case resp if resp.isError =>
        log.error(s"Elasticsearch error: ${resp.error}")
      case _ =>
    }
  }
}
