/*
 * Copyright 2019 Thinkinside s.r.l (thinkin.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.thinkin.kafka

import io.thinkin.kafka.store.OffsetStore
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

case class ConsumerLag(timestamp: Long, name: String, totalLag: Long, lagByTopicPartition: Map[TopicPartition, Long], offsetByTopicPartition: Map[TopicPartition, Long])

class KafkaLagMonitor(adminClient: AdminClient, consumer: KafkaConsumer[String, String], offsetStores: Iterable[OffsetStore])(implicit ec: ExecutionContext) {

  def computeLag(): Future[Iterable[ConsumerLag]] = {
    Future {
      val committedPartitionOffsetByGroup = offsetStores.iterator.map { os =>
        val osOffsets = os.getCommittedPartitionOffset()

        osOffsets.name -> osOffsets
      }.toMap

      val topicPartitions = committedPartitionOffsetByGroup.valuesIterator.flatMap(_.offsetMap.keys).toSet
      val topicPartitionsEndOffsets = consumer.endOffsets(topicPartitions.asJavaCollection)

      committedPartitionOffsetByGroup.map { case (name, committedPartitionOffset) =>
        val lagByTopicPartition = committedPartitionOffset.offsetMap.map { case (tp, committedOffset) =>
          val endOffset = topicPartitionsEndOffsets.getOrDefault(tp, committedOffset)
          val tpLag = endOffset - committedOffset
          tp -> tpLag
        }

        val totalLag = lagByTopicPartition.valuesIterator.sum

        ConsumerLag(committedPartitionOffset.timestamp, name, totalLag, lagByTopicPartition, committedPartitionOffset.offsetMap)
      }
    }
  }
}
