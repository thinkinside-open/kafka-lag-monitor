addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M13-4")
addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.12")
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.4.1")
addSbtPlugin("org.foundweekends" % "sbt-bintray" % "0.5.4")